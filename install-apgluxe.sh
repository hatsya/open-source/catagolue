#!/bin/bash
set -e

cd apgmera
./recompile.sh --symmetry "stdin"
cd ..

cp -r "apgmera/apgluxe" "initialise/apgluxe"

