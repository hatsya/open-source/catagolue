#!/bin/bash
set -ex

git clone "https://gitlab.com/apgoucher/apgmera.git"
git clone "https://gitlab.com/apgoucher/python-lifelib.git"

cd apgmera
bash update-lifelib.sh

cp ../python-lifelib/MANIFEST.in .
cp ../python-lifelib/setup.py .

pip3 install --user -e .
